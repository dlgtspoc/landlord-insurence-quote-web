package uk.co.dlg.ts.poc.sdlc.landlordinsurance

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LandlordInsuranceApplication

fun main(args: Array<String>) {
	runApplication<LandlordInsuranceApplication>(*args)
}
